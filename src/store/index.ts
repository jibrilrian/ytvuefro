import { createStore, Commit } from 'vuex'
import { VueCookieNext } from 'vue-cookie-next'

export default createStore({
  state: {
    authenticated: VueCookieNext.isCookieAvailable('name') ? true : false,
    name: VueCookieNext.getCookie('name')
  },
  mutations: {
    SET_AUTH: (state: {authenticated: boolean}, auth: boolean) => state.authenticated = auth
  },
  actions: {
    setAuth: ({commit}: {commit: Commit}, auth: boolean) => commit('SET_AUTH', auth)
  },
  modules: {
  }
})
